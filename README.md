GreekMe is an [Ionic](http://ionicframework.com/docs/) project utilizing Typescript, Angular, and a Firebase backend.

This project was built to solve the organization issue for Greek life at universities. Providing a means of role specific content sharing, chat, and event management. 
To be release on iOS and Android.

I am the only developer who worked on the product. Idea from Patrick Hopkins with MeTech llc.